package projectbank.presentation.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import projectbank.business.CustomerService;
import projectbank.domain.Customer;
import projectbank.presentation.view.ScreenHelper;


import javax.swing.*;
import java.io.IOException;
import java.net.URL;

public class RegisterController {
    @FXML
    private TextField inputLastName;

    @FXML
    private TextField inputFirstName;

    @FXML
    private TextField inputEmail;

    @FXML
    private TextField inputPassWord;
    @FXML
    private TextField inputCountry;
    @FXML
    private TextField inputAdress;
    @FXML
    private TextField inputZip;
    @FXML
    private TextField inputCity;
    @FXML
    private Text registerMessage;
    @FXML
    private TextField inputPhone;


    public void onClickLogin() {

        ScreenHelper.getSingleton().switchScreen("loginCustomer");


    }

    public void onClickConfirm() throws Exception {
        try{

            String lastName = inputLastName.getText();
          //  System.out.println(lastName);
            String firstName = inputFirstName.getText();
          //  System.out.println(firstName);
            String country = inputCountry.getText();
            String email = inputEmail.getText();
           // System.out.println(email);
            String passWord = inputPassWord.getText();
          //  System.out.println(passWord);
            String adress = inputAdress.getText();
            String zip = inputZip.getText();
            int zipInt = Integer.parseInt(zip);
            String city = inputCity.getText();
          //  System.out.println(firstName);
            String phone = inputPhone.getText();
            Customer customer = new Customer(lastName, firstName, country, email, passWord, adress, zipInt, phone,city);
            /**
             * here we call the methods to save customer in business package
             */
            CustomerService.createNewCustomer(customer);
           /* registerMessage.setText("Merci pour votre inscription,votre demande est bien enregistrer");
            registerMessage.getStyleClass().add("registerMessage");*/
            JOptionPane.showMessageDialog(null,"Merci pour votre inscription,votre demande est bien enregistrer");

            /**
             * here the method to clean the feild of the register form
             */
            clearFeild();
            ScreenHelper.getSingleton().switchScreen("loginCustomer");

        }

        catch (Exception e){
            JOptionPane.showMessageDialog(null,"veuillez remplir tout les champs");

        }
      //  ScreenHelper.getSingleton().switchScreen("validationAccount");

    }
    public void clearFeild(){
        inputFirstName.setText("");
        inputLastName.setText("");
        inputAdress.setText("");
        inputEmail.setText("");
        inputPassWord.setText("");
        inputZip.setText("");
        inputCity.setText("");
        inputCountry.setText("");
        inputPhone.setText("");
        inputLastName.requestFocus();
    }

    public void onClickCancel(ActionEvent actionEvent) {
       clearFeild();

    }

}
