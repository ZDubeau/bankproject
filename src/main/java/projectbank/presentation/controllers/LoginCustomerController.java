package projectbank.presentation.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import projectbank.business.LoginCustomerService;
import projectbank.domain.Customer;
import projectbank.presentation.view.ScreenHelper;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;

public class LoginCustomerController {

    @FXML
    private TextField inputMail;
    @FXML
    private TextField inputPassword;
    @FXML
    private Label errorMessage;





    public void onClickConfirm() throws IOException {

        Customer customer = null;
        try {
            /*//loading to homepageforeValidation and get the instance
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("homepageBeforeValidation.fxml"));
            Parent root = fxmlLoader.load();

            //get controller of homeBeforevalidationController
            homepageBeforeValidationController beforeValidationController = fxmlLoader.getController();

            beforeValidationController.showInformation(inputMail.getText());*/

            customer = LoginCustomerService.loginCustomer(inputMail.getText(), inputPassword.getText());

           // ScreenHelper.getSingleton().switchScreen("homepageBeforeValidation");

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (customer == null) {
            errorMessage.setText("adresse mail ou mot de pass incorrect, veuillez re-essayer.");
        }
        else {


            ScreenHelper.getSingleton().switchScreen("homepageBeforeValidation");
        }
    }

    public void onClickCancel() {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.show();
    }
}