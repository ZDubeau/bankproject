package projectbank.presentation.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import projectbank.business.AccountValidationService;
import projectbank.domain.Account;
import projectbank.domain.Banker;

import java.util.List;

public class ValidationAccountViewController {

    @FXML
    private GridPane gridPane;

    @FXML
    private ListView accountListView;

    ObservableList<Account> accountValidationToDisplay = FXCollections.observableArrayList();

    @FXML
    private void initialize() {
/*
        Label label1 = new Label("Label 1");
        Label label2 = new Label("Label 2");
        Button acceptButton = new Button("Accepter");
        Button declineButton = new Button("Refuser");

        HBox hbox = new HBox();
        hbox.getChildren().add(label1);
        hbox.getChildren().add(label2);

        gridPane.add(label1, 0, 1);
        gridPane.add(label2, 1, 1);
        gridPane.add(acceptButton, 2, 1);
        gridPane.add(declineButton, 3, 1);

        gridPane.addRow(1, label1, label2, acceptButton, declineButton);
*/
        accountListView.setItems(this.accountValidationToDisplay);
        /**
         * 2. Setup the text to be displayed in each list cell (the ListView object
         * cannot guess what to display from the Book object)
         */
        accountListView.setCellFactory(listView -> new ListCell<Account>() {
            @Override
            public void updateItem(Account account, boolean empty) {
                super.updateItem(account, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(account.getCustomerId() + " " + account.getAccountNumber());

                }
            }
        });

    }

    public void loadAllAccountValidationOnAction() {
        List<Account> accountList = AccountValidationService.listAllAccountValidation();
        this.accountValidationToDisplay.setAll(accountList);
    }

}
