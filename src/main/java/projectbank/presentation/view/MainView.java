package projectbank.presentation.view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class MainView extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        /**
         *  quentin's method
         */
        Parent homeScreen = FXMLLoader.load(getClass().getResource("/fxml/home.fxml"));
        Parent registerScreen = FXMLLoader.load(getClass().getResource("/fxml/register.fxml"));
        Parent loginCustomerScreen = FXMLLoader.load(getClass().getResource("/fxml/loginCustomer.fxml"));
        Parent homepageBeforeValidationScreen = FXMLLoader.load(getClass().getResource("/fxml/homepageBeforeValidation.fxml"));
     //   Parent customerdashboardScreen = FXMLLoader.load(getClass().getResource("/fxml/customerdashboard.fxml"));
     //   Parent loginBankerScreen = FXMLLoader.load(getClass().getResource("/fxml/loginBanker.fxml"));

        ScreenHelper screenHelper = ScreenHelper.getSingleton();
        screenHelper.addScreen("home", homeScreen);
        screenHelper.addScreen("register", registerScreen);
        screenHelper.addScreen("loginCustomer", loginCustomerScreen);
       screenHelper.addScreen("homepageBeforeValidation",homepageBeforeValidationScreen );
        /*screenHelper.addScreen("customerdashboard",customerdashboardScreen);
        screenHelper.addScreen("loginBanker", loginBankerScreen);*/
        Scene mainScene = new Scene(homeScreen);
        screenHelper.setMainScene(mainScene);

        primaryStage.setTitle("Project Bank");
        primaryStage.setScene(mainScene);
        primaryStage.setResizable(false);
        primaryStage.show();


    }

    public static void main(String[] args) {

        launch(args);
    }
}


