package projectbank.domain;

import java.util.Date;

public class Account {

    private int id;
    private int customerId;
    private Customer customer;
    private String accountNumber;
    private String iban;
    private float balance;
    private Date createdAt;
    private enum Status { VALIDE, EN_ATTENTE, REFUSE };

    public Account(int customerId, String accountNumber) {
        this.customerId = customerId;
        this.accountNumber = accountNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public int getCustomerId() {
        return customerId;
    }
}
