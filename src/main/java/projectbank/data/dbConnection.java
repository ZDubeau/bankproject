package projectbank.data;

import com.mysql.cj.jdbc.Driver;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;


public class dbConnection {

    public static Connection connection = null;

    public static void connecting() {
        try {

            String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
            String appConfigPath = rootPath + "application.properties";

            Properties appProps = new Properties();
            try {
                appProps.load(new FileInputStream(appConfigPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
            // Protocole de connexion
            String protocole = appProps.getProperty("protocole");
            String database = appProps.getProperty("database");
            String user = appProps.getProperty("user");
            String password = appProps.getProperty("password");
            String host = appProps.getProperty("host");
            String port = appProps.getProperty("port");

            // chargement de la classe par son nom
            Class c = Class.forName("com.mysql.jdbc.Driver");
            Driver pilote = (Driver) c.getDeclaredConstructor().newInstance();

            // enregistrement du pilote auprès du DriverManager
            DriverManager.registerDriver(pilote);

            // Chaîne de connexion
            String conString = protocole + "//" + host + ":" + port + "/" + database;
            // Connexion
            connection = DriverManager.getConnection(
                    conString, user, password);

        } catch (Exception e) {
            // gestion des exceptions
            System.out.println("Catch error in dbConnection : " + e);
        }
    }
}
