package projectbank.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectedDataBase {
    private static Connection connection;
    private static Statement stmt;
    public static Statement connectedDataBase() throws SQLException {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            connection= DriverManager.getConnection("jdbc:mysql://localhost/bankProject","marco","Sqlsimplon!");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            stmt = connection.createStatement();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return stmt;


    }
}
