package projectbank.data;

import projectbank.domain.Customer;

import java.sql.*;


public class customerDAO {
    private static Connection connection;

    public static Customer checkLogin(String mail, String password) throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost/bankProject", "root", "TakioTakio");
       // connection = DbConnection.getConnection();
        String sql = "SELECT * FROM Customer WHERE mail = ? and password = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, mail);
        statement.setString(2, password);

        ResultSet result = statement.executeQuery();

        Customer customer = null;

        if (result.next()) {
            customer = new Customer();
            customer.setLastName(result.getString("last_name"));
            customer.setMail(mail);
        }

        connection.close();

        return customer;
    }
}

