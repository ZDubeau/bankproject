package projectbank.data;

import com.mysql.cj.x.protobuf.MysqlxDatatypes;
import projectbank.domain.Customer;

import java.sql.*;

public class CustomerDAO {
    public static void addCustomer(Customer customer){
        /**
         *Connected with DATABASE
         *
         */
    /*    Statement stmt = null;
        try {
            stmt = ConnectedDataBase.connectedDataBase();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }*/
        /**
         * the sql request to add customer to the database
         */
        String tableName = "Customer";
        String lastName = customer.getLastName();
        String firstName = customer.getFirstName();
        String email = customer.getMail();
        String passWord = customer.getPassword();
        String adress = customer.getAddress();
        int zip = customer.getZip();
        String city = customer.getCity();
        String phone = customer.getPhone();
        String addcustomer = "INSERT INTO " + tableName + " (first_name, last_name,address,zip,city,country,phone,mail,password) VALUES ('" +firstName+ "','" + lastName+"','"+adress+ "','"+zip+ "','"+city+ "','"+"france"+ "','"+phone+ "','"+ email+ "','"+passWord+ "')";

        try {
            Statement statement = DbConnection.getStatement();
            int i = statement.executeUpdate(addcustomer);


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }
    public static Customer checkLogin(String mail, String password) throws SQLException, ClassNotFoundException {

        String sql = "SELECT * FROM Customer WHERE mail = ? and password = ?";
        PreparedStatement statement = DbConnection.getConnection().prepareStatement(sql);
        statement.setString(1, mail);
        statement.setString(2, password);

        ResultSet result = statement.executeQuery();

        Customer customer = null;

        if (result.next()) {
            customer = new Customer();
            customer.setLastName(result.getString("last_name"));
            customer.setMail(mail);
        }

        return customer;
    }
}
