package projectbank.data;

import projectbank.domain.Banker;

import java.sql.*;

public class LoginBankerDAO {


    public static Banker login(String mail, String password) throws SQLException {

        String sql = "SELECT * FROM Banker WHERE mail = ? and password = ?";
        PreparedStatement statement = DbConnection.getConnection().prepareStatement(sql);
        statement.setString(1, mail);
        statement.setString(2, password);

        ResultSet result = statement.executeQuery();

        Banker banker = null;

        if (result.next()) {
            banker = new Banker();
            banker.setFirstName(result.getString("first_name"));
            banker.setLastName(result.getString("last_name"));
        }

        return banker;
    }
}
