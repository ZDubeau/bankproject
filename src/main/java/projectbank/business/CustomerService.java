package projectbank.business;

import projectbank.data.CustomerDAO;
import projectbank.domain.Customer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class CustomerService {
    private static Connection connection;
    private static PreparedStatement preparedStatement;

    /**
     * the method to create a new customer
     * @param customer
     */

    public static void createNewCustomer(Customer customer){
        /**
         * send the customer input to the layer dao "CustomerDAO"
         */
        CustomerDAO.addCustomer(customer);

    }
}
