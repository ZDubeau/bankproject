package projectbank.business;

import projectbank.data.DbConnection;
import projectbank.data.LoginBankerDAO;
import projectbank.domain.Banker;

import java.sql.SQLException;

public class LoginBankerService {

    public static Banker loginBanker (String mail, String password) throws SQLException {

        Banker banker = null;

        banker = LoginBankerDAO.login(mail, password);

        // return true if is logged
        return banker;
    }
}
