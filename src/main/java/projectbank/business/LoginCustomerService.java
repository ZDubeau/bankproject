package projectbank.business;

import projectbank.data.CustomerDAO;
import projectbank.data.customerDAO;
import projectbank.data.dbConnection;
import projectbank.domain.Customer;

import java.sql.SQLException;

public class LoginCustomerService {

    public static Customer loginCustomer (String mail, String password) throws SQLException {


        Customer customer = null;

        try {
           customer = CustomerDAO.checkLogin(mail, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        // return true if is logged
        return customer;
    }
}
