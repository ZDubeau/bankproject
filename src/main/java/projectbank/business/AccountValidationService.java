package projectbank.business;

import projectbank.domain.Account;
import projectbank.domain.Banker;
import projectbank.domain.Customer;

import java.util.ArrayList;
import java.util.List;

public class AccountValidationService {

    public static List<Account> listAllAccountValidation() {

        List<Account> accountList = new ArrayList<>();
        Account account = new Account(1, "12 34 56");
        Account account1 = new Account(2, "77 88 99");
        accountList.add(account);
        accountList.add(account1);
        return accountList;
    }
}
